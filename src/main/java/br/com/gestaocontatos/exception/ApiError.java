package br.com.gestaocontatos.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers.DateDeserializer;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;

public class ApiError {

	 private HttpStatus status;
	 private int statusCode;
	   private Date timestamp;
	   private String message;
	   private String debugMessage;
	   
	   
	   private ApiError() {
	       timestamp = new Date();
	   }

	   ApiError(HttpStatus status) {
	       this();
	       this.status = status;
	       this.statusCode = status.value();
	   }

	   ApiError(HttpStatus status, Throwable ex) {
	       this();
	       this.status = status;
	       this.statusCode = status.value();
	       this.message = "Unexpected error";
	       this.debugMessage = ex.getLocalizedMessage();
	   }

	   ApiError(HttpStatus status, String message, Throwable ex) {
	       this();
	       this.status = status;
	       this.statusCode = status.value();
	       this.message = message;
	       this.debugMessage = ex.getLocalizedMessage();
	   }
	   
	   ApiError(HttpStatus status, String message, String debugMessage) {
	       this();
	       this.status = status;
	       this.statusCode = status.value();
	       this.message = message;
	       this.debugMessage = debugMessage;
	   }

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	@JsonSerialize(using = DateSerializer.class)
	public Date getTimestamp() {
		return timestamp;
	}

	@JsonDeserialize(using = DateDeserializer.class)
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDebugMessage() {
		return debugMessage;
	}

	public void setDebugMessage(String debugMessage) {
		this.debugMessage = debugMessage;
	}


	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	
}
