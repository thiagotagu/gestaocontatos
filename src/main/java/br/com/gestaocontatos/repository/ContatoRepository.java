package br.com.gestaocontatos.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gestaocontatos.entity.Contato;

	
	@Repository
	@Transactional
	public interface ContatoRepository extends JpaRepository<Contato, Long> {


}
