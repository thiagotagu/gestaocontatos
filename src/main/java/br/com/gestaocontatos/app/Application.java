package br.com.gestaocontatos.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import br.com.gestaocontatos.service.ContatoService;


@ComponentScan(basePackages = {"br.com.gestaocontatos","br.com.gestaocontatos.controller","br.com.gestaocontatos.service"})
@EnableJpaRepositories(basePackages = {"br.com.gestaocontatos.repository"})
@EntityScan(basePackages = {"br.com.gestaocontatos.entity"})
@SpringBootApplication
public class Application implements ApplicationListener<ContextRefreshedEvent> {
	
	@Autowired
	ContatoService contatoService;
	
	public static void main(String[] args){
	    SpringApplication.run(Application.class, args);
	    
	 
	}

	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		   contatoService.CriaDados();
		
	}
	
}
