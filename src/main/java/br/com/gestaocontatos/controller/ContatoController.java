package br.com.gestaocontatos.controller;

import java.sql.SQLException;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gestaocontatos.entity.Contato;
import br.com.gestaocontatos.service.ContatoService;

@RestController
@RequestMapping("contato")
public class ContatoController {
	
	
	@Autowired
	ContatoService contatoService;
	
	@GetMapping("{id}")
	public @ResponseBody ResponseEntity<?> get(@PathVariable Long id) {
		return new ResponseEntity<Optional<Contato>>(contatoService.findById(id), HttpStatus.OK);
	}

	@GetMapping
	public @ResponseBody ResponseEntity<?> obterTodos(Long paginaAtual, Long tamanho) {
		
		return new ResponseEntity<Page<Contato>>(contatoService.paginacao(paginaAtual,tamanho), HttpStatus.OK);
	}

	@PostMapping
	public @ResponseBody ResponseEntity<?> add(@RequestBody @Valid Contato contato) throws SQLException {
		return new ResponseEntity<Contato>(contatoService.gravar(contato), HttpStatus.OK);

	}

	@PutMapping
	public @ResponseBody ResponseEntity<?> atualizar(@RequestBody @Valid Contato contato) {
		return new ResponseEntity<Contato>(contatoService.gravar(contato), HttpStatus.OK);
	}

	@DeleteMapping("{id}")
	public @ResponseBody ResponseEntity<?> delete(@PathVariable Long id) {
		return new ResponseEntity<Boolean>(contatoService.delete(id), HttpStatus.OK);
	}

}
