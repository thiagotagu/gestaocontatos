package br.com.gestaocontatos.service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.hibernate.service.spi.ServiceException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

public abstract class AbstractService<T, PK extends Serializable> implements Serializable {

		private static final long serialVersionUID = 1L;

		 
		@Transactional(readOnly = true)
		public List<Optional<T>> findAll() throws ServiceException {
			try {
				return (List<Optional<T>>) getRepository().findAll();

			} catch (Exception ex) {
				throw new ServiceException("Erro ao buscar dados", ex);

			}
		}
		
		
		@Transactional(readOnly = true)
		public Optional<T> findById(PK id ) throws ServiceException {
			try {
				return   getRepository().findById(id);

			} catch (Exception ex) {
				throw new ServiceException("Erro ao buscar dados", ex);

			}
		}

		 
		@Transactional(readOnly = true)
		public Page<T> findAll(int page, int pageSize, Sort sort) throws ServiceException {
			try {
				return (Page<T>) getRepository().findAll(new PageRequest(page, pageSize, sort));
			} catch (Exception ex) {
				throw new ServiceException("Erro ao buscar dados", ex);

			}
		}

		
		 
		public List<T> saveAll(List<T> lst) throws ServiceException {
			try {
				getRepository().saveAll(lst);
				return lst; 
			} catch (Exception ex) {
				throw new ServiceException(ex.getMessage(), ex);
			}
		}

		 
		public T save(T entity) throws ServiceException {
			try {
				getRepository().save(entity);
				return entity;
			} catch (Exception ex) {
				throw new ServiceException(ex.getMessage(), ex);
			}
		}

		 
		public boolean update(Optional<T> entity) throws ServiceException {
			try {
				if (entity.isPresent()) {
					getRepository().save(entity.get());
					return true; 
				}
			} catch (Exception ex) {
				throw new ServiceException(ex.getMessage(), ex);
			}
			return false;
		}
 
		public boolean delete(Optional<T> entity) throws ServiceException {
			try {
				if (entity.isPresent()) {
				getRepository().delete(entity.get());
				return true;
				}
			} catch (Exception ex) {
				throw new ServiceException(ex.getMessage(), ex);
			}
			return false;
		}


		 
		public Boolean delete(PK id) throws ServiceException {
			try {
				getRepository().deleteById(id);
				return true;
			} catch (Exception ex) {
				throw new ServiceException(ex.getMessage(), ex);
			}
		}

		public abstract PagingAndSortingRepository<T, PK> getRepository();

}
