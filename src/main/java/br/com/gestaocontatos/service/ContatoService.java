package br.com.gestaocontatos.service;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import br.com.gestaocontatos.entity.CanalEnum;
import br.com.gestaocontatos.entity.Contato;
import br.com.gestaocontatos.repository.ContatoRepository;
import br.com.gestaocontatos.util.Util;

	@Service
	public class ContatoService extends AbstractService<Contato, Long> {
		private static final long serialVersionUID = 1L;
		@Autowired
		ContatoRepository contatoRepository;
		
		@Override
		public PagingAndSortingRepository<Contato, Long> getRepository() {
			// TODO Auto-generated method stub
			return contatoRepository;
		}
		public void CriaDados() {
			
			Contato contato = new Contato();
			Contato contato1 = new Contato();
			Contato contato2 = new Contato();
			Contato contato3 = new Contato();
			List<Contato> lstDados = new ArrayList<Contato>();
					
			contato.setNome("teste contato 1");
			contato.setCanal(CanalEnum.EMAIL);
			contato.setObservacao("teste 1 ");
			contato.setValor("teste@gmail.com");
			lstDados.add(contato);
			
			 contato1.setNome("teste contato 2");
			contato1.setCanal(CanalEnum.CELULAR);
			contato1.setObservacao("teste 2 ");
			contato1.setValor("979546814");
			lstDados.add(contato1);
			 
			contato2.setNome("teste contato 3");
			contato2.setCanal(CanalEnum.CELULAR);
			contato2.setObservacao("teste 3 ");
			contato2.setValor("44887455");
			lstDados.add(contato2);
			 
			contato3.setNome("teste contato 4");
			contato3.setObservacao("teste 4 ");
			contato3.setValor("7782211");
			contato3.setCanal(CanalEnum.FIXO);
			lstDados.add(contato3); 
			 
			saveAll(lstDados);
			
		}
		public Contato gravar(@Valid Contato contato) {
			if (contato.getCanal() == null ) {
				throw new ServiceException("Favor informar o Canal.");
			}
			
			if (contato.getCanal().equals(CanalEnum.EMAIL) && !Util.validarEmail(contato.getValor())) {
				throw new ServiceException("Email invalido.");
			}
			return save(contato);
		}
		
		public Page<Contato> paginacao(Long pgAtual,Long tamanho) {
			if (pgAtual == null) {
				pgAtual = 0L;
			}
			if (tamanho == null || tamanho == 0) {
				tamanho = 10L;
			}
	        return findAll(pgAtual.intValue(),tamanho.intValue(),new Sort(Sort.Direction.ASC, "id"));
	    }
	
	
}
