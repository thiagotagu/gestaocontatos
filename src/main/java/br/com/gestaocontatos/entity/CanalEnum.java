package br.com.gestaocontatos.entity;

public enum  CanalEnum {

	
	EMAIL("EMAIL"), CELULAR("CELULAR"), FIXO("FIXO");

	private String canalEnum;

	private CanalEnum(String canalEnum) {
		this.canalEnum = canalEnum;
	}

	public String getcanalEnum() {
		return this.canalEnum;
	}
}
