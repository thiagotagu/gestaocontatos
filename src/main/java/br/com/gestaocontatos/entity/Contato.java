package br.com.gestaocontatos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.ColumnTransformer;

@Entity
@Table(name = "CONTATO")
@SequenceGenerator(name = "SEQ_CONTATO", sequenceName = "SEQ_CONTATO", allocationSize = 1)
public class Contato {

	@Id
	@Column(name = "id")
	@GeneratedValue(generator = "SEQ_CONTATO", strategy = GenerationType.AUTO)
	Long id;
	
	@NotEmpty(message="Favor informar o Nome")
	@Column(name = "NOME", length = 100)
	String nome;

 
	@Column(name = "CANAL",length = 30)	
	@ColumnTransformer(write  = "UPPER(?)")
	@Enumerated(EnumType.STRING)
	CanalEnum canal;
	 
	@Column(name = "OBSERVACAO",length = 500)
	String observacao;
	
	@Column(name = "VALOR")
	String valor;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public CanalEnum getCanal() {
		return canal;
	}

	public void setCanal(CanalEnum canal) {
		this.canal = canal;
	}
}
